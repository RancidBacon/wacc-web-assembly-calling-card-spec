## Web Assembly Calling Card (WACC) Specification v0.1

_Like an Avatar but WASM!_


 * Turn unsigned 32-bit integers into coloured triangles!
 * Make embedding/implementing WebAssembly runtimes more exciting.
 * Support cross-device/application, sandboxed, dynamically-generated, vector animated (currently non-interactive) and technically constrained personal expression.


![WebAssembly Calling Card logo](assets/wacc-logo-with-text--smaller.png)


### Audience

 * People who wish to have a personalized dynamic avatar in e.g. an online game environment.
 * People who like creatively expressing themselves in technically constrained environments.
 * Developers who are embedding or implementing WebAssembly runtimes.
 * Developers who wish to allow e.g. online game players to create & use a personalized dynamic avatar/icon/brand/flag in-game but contained within a sandboxed environment.
 * Developers who wish to create tools to enable people to create and/or customise their own WACC.


### Context

The initial WACC concept was formed while I was integrating a WebAssembly (WASM) runtime with a game engine. The first code example that I got running returned an unsigned 32-bit integer value. And--rather than spend time integrating additional runtime functionality--I decided to turn such integers into triangles for a more interesting first-run "hello world" experience. :)


### Specification

##### WASM Module Requirements

 * A WebAssembly Calling Card is contained within a single WebAssembly module binary file (i.e. `.wasm`).

 * For identification purposes the "full" file extension can be `.wacc.wasm`.  
   *(Considered only using `.wacc` for the extension but decided against it for now.)*

 * An exported function named `wacc` must be present in the module.  
   A WACC viewer implementation *should* search the list of all exported functions.  
   *(For ease of implementation it's probably good if the `wacc()` function is the first export/function/external but it's not required.)*
   
 * The exported `wacc` function *must* have a function prototype/signature that accepts no arguments & returns one unsigned 32-bit integer value each time it is called.  
   *(See "Data Format" section below for specification of how these returned values will be interpreted.)*

 * The module *must not* require any WebAssembly *imports* to be supplied.


##### Data Format

* Basic

    ```
    [8b][8b][1b][5b][5b][5b]
    [x1][y1][a][r][g][b]
    [x2][y2]
    [x3][y3]
    ```


### Examples

 * A zip file with examples & source are available as a download with the WACC Viewer download page.


### DIY: Online

To avoid having to install a WebAssembly tool chain you can use one of the following services to compile C/C++ to WASM:

 * <https://wasdk.github.io/WasmFiddle/>

 * <https://anonyco.github.io/WasmFiddlePlusPlus/>

 * <https://mbebenita.github.io/WasmExplorer/>


### Viewer/Renderer Implementations

##### Current

 * Desktop: [WACC Viewer download](https://rancidbacon.itch.io/wacc-viewer) ([source](https://gitlab.com/RancidBacon/wacc-viewer)) -- demo app & re-usable component.

 * Web: [WACC Web Viewer](https://wacc.rancidbacon.com/viewer) ([source](https://gitlab.com/RancidBacon/wacc-web-viewer)) -- JavaScript & Canvas (doesn't yet support per-vertex color).

##### Future possibilities

 * Embedded systems with displays #badgelife

## More details soon...
